import Vue from "vue";
import Router from "vue-router";
import Clients from "./views/Clients.vue";
import ClientReferences from "./components/clients/ClientReferences.vue";
import ClientForm from "./components/clients/ClientForm.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      redirect: { name: "client" }
    },
    {
      path: "/client/",
      name: "client",
      component: Clients,
      children: [
        {
          path: "edit",
          name: "client-edit",
          component: ClientForm
        }
      ]
    },
    {
      path: ":id/references",
      name: "client-references",
      component: ClientReferences
    },
    {
      path: "/client/new",
      name: "new-client",
      component: ClientForm
    }
  ]
});
