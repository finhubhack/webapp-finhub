const w3w_api = require("@what3words/api");
w3w_api.setOptions({ key: process.env.VUE_APP_W3W_KEY });

export default w3w_api;
