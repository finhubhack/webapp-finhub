import Vue from "vue";
import Element from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import "element-ui/lib/theme-chalk/display.css";
import "element-ui/packages/theme-chalk/src/common/var.scss";
import "../element-variables.scss";
import locale from "element-ui/lib/locale/lang/es";

Vue.use(Element, { locale });
